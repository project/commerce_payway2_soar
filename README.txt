Overview
========
PayWay2 SOAR gateway support for Drupal Commerce module.

Summary
=======
Adds a payment methods for italian PayWay2 SOAR

Supported
 * Modalità Base Release 1.2.

Todo
 * Modalità Avanzata Release 2.0.

Methods
=======
Most implemented through payment forms.
